package ly.journa.journly;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sendgrid.SendGrid;

import com.sendgrid.SendGridException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import ly.journa.journly.Class.Constant;

/**
 * Created by acer on 9/27/2017.
 */

public class ForgotPassword extends AppCompatActivity {

    TextView remember_ps, error_msg_forgot;
    Button reset;
    EditText email_f;
    Typeface tf;
    Toolbar toolbar;
    Context context;
    String email_;
    ProgressDialog pd;
    Random r = new Random();
    ProgressDialog progressDialog;
    RelativeLayout relativeLayout;

    SessionManagement sessionManagement;
    SessionManager sess;
    Animation fadein;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        setTitle("");

        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        sessionManagement = new SessionManagement(getApplicationContext());
        sess = new SessionManager(getApplicationContext());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(ForgotPassword.this);

        pd = new ProgressDialog(ForgotPassword.this);

        remember_ps = (TextView) findViewById(R.id.remember_ps);
        reset = (Button) findViewById(R.id.reset_password);
        email_f = (EditText) findViewById(R.id.email_forgot);
        error_msg_forgot = (TextView) findViewById(R.id.error_msg_forgot);
        relativeLayout = (RelativeLayout)findViewById(R.id.no_account_forgot);

        fadein = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        tf = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        remember_ps.setTypeface(tf);
        reset.setTypeface(tf);
        email_f.setTypeface(tf);
        error_msg_forgot.setTypeface(tf);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email_ = email_f.getText().toString().trim();
                Log.e("Email:", email_);

                int new_ps = r.nextInt(10000 - 10) + 10000;


                CheckEmailUser up = new CheckEmailUser();
                up.execute(email_, String.valueOf(new_ps));

            }
        });
    }
    class CheckEmailUser extends AsyncTask<String, String, String>{

        String EMAIL__;
        String PS__;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading...");
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            EMAIL__ = params[0];
            PS__ = params[1];

            String data = "";
            int tmp;

            try {
                URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/check_email");
                String urlParams = "email="+EMAIL__;


                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }

        }


        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();


            Log.e("RESPONSE-->>", response);

            try {
               String cd_ = "";

                if (response.isEmpty()){

                    relativeLayout.startAnimation(fadein);
                    relativeLayout.setVisibility(View.VISIBLE);
                    error_msg_forgot.setText("Please enter your registered email.");

                }
                else{

                    JSONObject json = new JSONObject(response);


                    //String status = json.getString("status");
                    JSONObject created_date = json.getJSONObject("result");
                    cd_ = created_date.getString("created_date");

                    SendEmailASyncTask task = new SendEmailASyncTask(EMAIL__, String.valueOf(PS__), cd_);
                    task.execute();
                }
                //Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();


            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }

        }
    }

  class SendEmailASyncTask extends AsyncTask<Void, Void, Void> {

        private Context mAppContext;
        private String mMsgResponse;

         String mTo, n_ps, cd;


      public SendEmailASyncTask(String mTo, String n_ps, String oid) {
          this.mTo = mTo;
          this.n_ps = n_ps;
          this.cd = oid;
      }

      @Override
      protected void onPreExecute() {
          super.onPreExecute();

      }

      @Override
        protected Void doInBackground(Void... params) {

            try {
                SendGrid sendgrid = new SendGrid("jonwoon", "asdf1234");

                SendGrid.Email email = new SendGrid.Email();

                email.addTo(mTo);
                email.setFrom("support@journa.ly");
                email.setSubject("Journa.ly - Forgot Password");
                Resources res = getResources();

                StringBuilder stringBuilder;

                String new_ps = n_ps;

                String mystring ="Hey,\n\nThis is your new password - "+ new_ps +"\n\nYou'll be asked for a new password once you login with this temporary one. Please have a new one ready to go.\n\nThanks,\nJourna.ly";

                email.setText(mystring);


                // Send email, execute http request
                SendGrid.Response response = sendgrid.send(email);
                mMsgResponse = response.getMessage();

                Log.d("SendAppExample", mMsgResponse);

            } catch (SendGridException e) {
                Log.e("SendAppExample", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            UpdatePassword up = new UpdatePassword();
            up.execute(n_ps, mTo, cd);
            progressDialog.dismiss();
            //Toast.makeText(ForgotPassword.this, mMsgResponse, Toast.LENGTH_SHORT).show();
        }
    }



    class UpdatePassword extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog = new ProgressDialog(ForgotPassword.this);

        String PASSWORD_, EMAIL_THIS, CD_;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading...");
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            PASSWORD_ = params[0];
            EMAIL_THIS = params[1];
            CD_ = params[2];
            String data = "";
            int tmp;


            Log.e("STRING", EMAIL_THIS + PASSWORD_ );


            try {
                URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/updatePassword");
                String urlParams = "email="+EMAIL_THIS+"&password="+PASSWORD_+"&passcode="+"1";


                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }


        }


        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            progressDialog.dismiss();

            Log.e("STATUS", response);

            if (response.equals("1")){
                //sessionManagement.createLoginSession(EMAIL_THIS, PASSWORD_, CD_);
                //sess.setLogin(true);
                progressDialog.dismiss();
                Intent i = new Intent(ForgotPassword.this, MainActivity.class);
                startActivity(i);
                finish();
            }else {
                Toast.makeText(ForgotPassword.this, "Password was not updated!", Toast.LENGTH_SHORT).show();
            }


        }


    }

    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
