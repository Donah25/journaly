package ly.journa.journly;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ly.journa.journly.Class.Constant;
import ly.journa.journly.Class.Journals;

/**
 * Created by acer on 9/27/2017.
 */

public class JournalActivity extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, SwipeRefreshLayout.OnRefreshListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    Button btnAdd;
    Typeface tf;
    TextView txt_expired, txt_continue;
    Button btn_subscribe;
    List<Journals> journals = new ArrayList<>();
    RecyclerView recyclerView;
    CustomAdapter customAdapter;
    HashMap<String, List<Journals>> groupedHashMap = new HashMap<>();
    SessionManagement sessionManagement;
    String email, date_created;
    private Paint p = new Paint();
    private View view;
    int numdays;
    private SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout relativeLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_home, container, false);
        //getSupportActionBar().setElevation(0);

        setHasOptionsMenu(true);

        sessionManagement = new SessionManagement(getActivity());

        HashMap<String, String> user = sessionManagement.getUserDetails();

        email = user.get(SessionManagement.KEY_EMAIL);
        date_created = user.get(SessionManagement.date_created_);

        txt_expired = (TextView) rootView.findViewById(R.id.msg_trial_expired);
        txt_continue = (TextView) rootView.findViewById(R.id.continue_txt);
        btn_subscribe = (Button) rootView.findViewById(R.id.btn_subscribe);
        relativeLayout = (RelativeLayout) rootView.findViewById(R.id.trial_expired);

        tf = Typeface.createFromAsset(getActivity().getAssets(), "perpetua.ttf");


        txt_continue.setTypeface(tf);
        txt_expired.setTypeface(tf);
        btn_subscribe.setTypeface(tf);



        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

       // new getData().execute();

        System.out.println("Current time => " + date_created);

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        new getData().execute();
                                    }
                                }
        );

        try {

            //yyyy-mm-dd
            String datee = date_created;

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date todayDate = new Date();
            String thisDate = df.format(todayDate);

            String parts1[] = thisDate.split("-");

            int day1 = Integer.parseInt(parts1[2]);
            int month1 = Integer.parseInt(parts1[1]);
            int year1 = Integer.parseInt(parts1[0]);

            int resultMonth = month1 - 1;

            String currentDate = year1 + "-" + month1 + "-" + day1;

            Date set_date = null;
            Date current_date = null;

            set_date = df.parse(datee);
            current_date = df.parse(currentDate);

            //86400000
            long diff = current_date.getTime() - set_date.getTime() ;

            long days = diff / (24 * 60 * 60 * 1000);

            numdays = (int)days;


            Log.d("NUMBER OF DAYS: ", Integer.toString(numdays) + " " + datee + " " + currentDate);

        } catch (ParseException e) {
            Log.e("TEST", "Exception", e);
        }

        if (numdays >= 15){
            relativeLayout.setVisibility(View.VISIBLE);
        }


        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), UpgradeActivity.class);
                startActivity(i);
            }
        });


        return rootView;
    }


    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        new getData().execute();
    }



class getData extends AsyncTask<String, String, String> {

    String stream = null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        swipeRefreshLayout.setRefreshing(true);
        journals.clear();

    }

    @Override
    protected String doInBackground(String... params) {

        String data = "";
        int tmp;

        try {
            URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/ViewJournals/"+ email);


            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            OutputStream os = httpURLConnection.getOutputStream();
            os.flush();
            os.close();

            InputStream is = httpURLConnection.getInputStream();
            while ((tmp = is.read()) != -1) {
                data += (char) tmp;
            }

            is.close();
            httpURLConnection.disconnect();

            return data;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Exception: " + e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return "Exception: " + e.getMessage();
        }
    }


    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        swipeRefreshLayout.setRefreshing(false);

        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo nf = manager.getActiveNetworkInfo();

        if (nf == null) {
            Toast.makeText(
                    getActivity(),
                    "No internet connection",
                    Toast.LENGTH_LONG).show();
        } else {

            try {

                JSONObject json = new JSONObject(response);

                String result = json.getString("result");

                JSONArray jsonArray = new JSONArray(result);


                for (int i=0; i<jsonArray.length(); i++) {
                    Journals journals1 = new Journals();
                    JSONObject obj = jsonArray.getJSONObject(i);



                        journals1.journal_name = obj.getString("title");
                        journals1.thoughts = obj.getString("thoughts");
                        journals1.date_created = obj.getString("created_date");
                        journals1.images_stock = obj.getString("img_name");


                        journals1.journal_id = obj.getString("journal_id");

                        Log.e("data=======>>", email);
                        //journals1.journal_name = obj.getString("title");

                        journals.add(journals1);




                }



                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                customAdapter = new CustomAdapter(getActivity(), journals);
                recyclerView.setAdapter(customAdapter);

                customAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }

        }

    }

}

    private void removeView(){
        if(view.getParent()!=null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }

    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {


        if (direction == ItemTouchHelper.LEFT){
            customAdapter.removeItem(position);
        } else {
            removeView();

        }
    }

   /* private HashMap<String, List<Journals>> groupDataIntoHashMap(List<Journals> listOfPojosOfJsonArray) {

        for (Journals pojoOfJsonArray : listOfPojosOfJsonArray) {

            String hashMapKey = pojoOfJsonArray.getDate_created();

            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(pojoOfJsonArray);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                List<Journals> list = new ArrayList<>();
                list.add(pojoOfJsonArray);
                groupedHashMap.put(hashMapKey, list);
            }
        }


        return groupedHashMap;
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {



        getActivity().getMenuInflater().inflate(R.menu.search_menu, menu);
        getActivity().getMenuInflater().inflate(R.menu.add_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        searchEditText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        searchEditText.setHintTextColor(getResources().getColor(R.color.colorPrimaryDark));
        searchEditText.setTypeface(tf);


        SpannableStringBuilder title = new SpannableStringBuilder(getContext().getString(R.string.search));
        title.setSpan(tf, 0, title.length(), 0);
        MenuItem menuItem = menu.findItem(R.id.action_search); // OR THIS
        menuItem.setTitle(title);


        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {

            @Override
            public boolean onQueryTextChange(String query) {

                final List<Journals> filteredModelList = filter(journals, query);

                customAdapter.setFilter(filteredModelList);

                return true;

            }
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

        });


        super.onCreateOptionsMenu(menu, inflater);


    }

    private List<Journals> filter(List<Journals> models, String query) {
        query = query.toLowerCase();
        final List<Journals> filteredModelList = new ArrayList<>();
        for (Journals model : models) {
            final String text = model.date_created.toLowerCase();
            final String cname = model.journal_name.toLowerCase();
            if (text.contains(query) || cname.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.action_search:   //this item has your app icon
                return true;
            case R.id.action_plus:
                Intent intent = new Intent(getActivity(), AddJournalActivity.class);
                startActivity(intent);
                return true;

            default: return super.onOptionsItemSelected(item);
        }
    }


    public interface OnBackPressedListener {
        void onBackPressed();
    }




}
