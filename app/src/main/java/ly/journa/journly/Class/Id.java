package ly.journa.journly.Class;

import com.google.gson.annotations.SerializedName;

/**
 * Created by acer on 9/27/2017.
 */

public class Id {
    @SerializedName("$oid")

    public String oid;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }
}
