package ly.journa.journly.Class;

/**
 * Created by acer on 9/27/2017.
 */

public class Journals {
    public  Id _oid;
    public  String journal_id;
    public  String user_id;
    public  String journal_name;
    public  String date_created;
    public  String images_stock;
    public  String thoughts;

    public Journals() {
    }

    public Journals(String user_id, String journal_name, String date_created, String images_stock, String thoughts) {
        this.user_id = user_id;
        this.journal_name = journal_name;
        this.date_created = date_created;
        this.images_stock = images_stock;
        this.thoughts = thoughts;
    }

    public Id get_oid() {
        return _oid;
    }

    public void set_oid(Id _oid) {
        this._oid = _oid;
    }

    public String getThoughts() {
        return thoughts;
    }

    public void setThoughts(String thoughts) {
        this.thoughts = thoughts;
    }

    public String getJournal_id() {
        return journal_id;
    }

    public void setJournal_id(String journal_id) {
        this.journal_id = journal_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getJournal_name() {
        return journal_name;
    }

    public void setJournal_name(String journal_name) {
        this.journal_name = journal_name;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getImages_stock() {
        return images_stock;
    }

    public void setImages_stock(String images_stock) {
        this.images_stock = images_stock;
    }
}
