package ly.journa.journly.Class;

/**
 * Created by acer on 9/30/2017.
 */

public abstract class ListItem {

    public static final int TYPE_DATE = 0;
    public static final int TYPE_GENERAL = 1;

    abstract public int getType();
}
