package ly.journa.journly.Class;

/**
 * Created by acer on 9/28/2017.
 */

public class PojoAlarm {

    public  String alarm;
    public  String UCODE;
    public int _id;

    public PojoAlarm() {
    }

    public PojoAlarm(String alarm, String UCODE) {
        this.alarm = alarm;
        this.UCODE = UCODE;
    }

    public String getAlarm() {
        return alarm;
    }

    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }

    public String getUCODE() {
        return UCODE;
    }

    public void setUCODE(String UCODE) {
        this.UCODE = UCODE;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
}
