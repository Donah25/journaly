package ly.journa.journly.Class;

/**
 * Created by acer on 9/27/2017.
 */

public class Users {
    public  Id user_id;
    public  String email;
    public  String password;
    public  String passcode;
    public  String alarm;
    public  String paying_user;
    public  String date_created;


    public Id getUser_id() {
        return user_id;
    }

    public void setUser_id(Id user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public String getAlarm() {
        return alarm;
    }

    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }

    public String getPaying_user() {
        return paying_user;
    }

    public void setPaying_user(String paying_user) {
        this.paying_user = paying_user;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }
}
