package ly.journa.journly.Class;

/**
 * Created by acer on 9/30/2017.
 */

public class GeneralItem extends ListItem {
    private Journals pojoOfJsonArray;

    public Journals getPojoOfJsonArray() {
        return pojoOfJsonArray;
    }

    public void setPojoOfJsonArray(Journals pojoOfJsonArray) {
        this.pojoOfJsonArray = pojoOfJsonArray;
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }




}