package ly.journa.journly.Class;

/**
 * Created by acer on 9/27/2017.
 */

public class Months {
    public String months;
    public  String months_id;
    public  String num_days;
    public  String leapyear;

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    public String getMonths_id() {
        return months_id;
    }

    public void setMonths_id(String months_id) {
        this.months_id = months_id;
    }

    public String getNum_days() {
        return num_days;
    }

    public void setNum_days(String num_days) {
        this.num_days = num_days;
    }

    public String getLeapyear() {
        return leapyear;
    }

    public void setLeapyear(String leapyear) {
        this.leapyear = leapyear;
    }
}
