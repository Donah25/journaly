package ly.journa.journly.Class;

/**
 * Created by acer on 9/28/2017.
 */

public class PojoPasscode {
    public  String passcode;
    public  String UCODE;
    public int _id;

    public PojoPasscode() {
    }


    public PojoPasscode(String passcode, String UCODE) {
        this.passcode = passcode;
        this.UCODE = UCODE;
    }


    public String getUCODE() {
        return UCODE;
    }

    public void setUCODE(String UCODE) {
        this.UCODE = UCODE;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }


    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }
}
