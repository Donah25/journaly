package ly.journa.journly.Class;

/**
 * Created by acer on 9/30/2017.
 */

public class DateItem extends ListItem {
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public int getType() {
        return TYPE_DATE;
    }

}


