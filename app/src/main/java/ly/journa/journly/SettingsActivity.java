package ly.journa.journly;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import ly.journa.journly.Class.PojoAlarm;
import ly.journa.journly.Class.PojoPasscode;

/**
 * Created by acer on 9/27/2017.
 */

public class SettingsActivity extends AppCompatActivity {

    Toolbar toolbar;
    Typeface tf;
    TextView forgotps, theme;
    Switch alarm, passcode;
    EditText email, password;
    Dbhandler dbhandler;
    PojoPasscode pojoPasscode_alarm;
    SessionManagement session;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle("");

        session = new SessionManagement(getApplicationContext());
        dbhandler = new Dbhandler(SettingsActivity.this);
        final List<PojoAlarm> pData = dbhandler.getAlarm();

        HashMap<String, String> user = session.getUserDetails();

        String PS = user.get(SessionManagement.KEY_PS);

        String email_ = user.get(SessionManagement.KEY_EMAIL);

        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        forgotps =  (TextView)findViewById(R.id.forgot_passwords);
        theme =  (TextView)findViewById(R.id.theme_settings);
        alarm =  (Switch) findViewById(R.id.alarm);
        passcode =  (Switch)findViewById(R.id.passcode);
        email =  (EditText) findViewById(R.id.email_settings);
        password =  (EditText) findViewById(R.id.ps_settings);


        tf  = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        forgotps.setTypeface(tf);
        theme.setTypeface(tf);
        alarm.setTypeface(tf);
        passcode.setTypeface(tf);
        email.setTypeface(tf, Typeface.ITALIC);
        password.setTypeface(tf, Typeface.ITALIC);

        email.setText(email_);
        password.setText(PS);

        forgotps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, ForgotPassword.class);
                startActivity(intent);
            }
        });

        for (PojoAlarm cn : pData) {
            if (cn.getAlarm().equals("1")){
                alarm.setChecked(true);
            }
            Log.e("Alarm ---->", cn.getAlarm());
        }

        final List<PojoPasscode> pcode = dbhandler.getPasscode();

        for (PojoPasscode ps : pcode) {
            if (!ps.getPasscode().equals("0")) {
                passcode.setChecked(true);
            }
        }
        alarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true){
                    dbhandler.updateAlarm("1", "donah");
                }else {
                 dbhandler.updateAlarm("0", "donah");


                }
            }
        });

        passcode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true){
                    Intent intent = new Intent(SettingsActivity.this, AddPinCodeActivity.class);
                    startActivity(intent);


                }else {
                   dbhandler.updatePasscode("0", "donah");
                }
            }
        });
    }
    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
