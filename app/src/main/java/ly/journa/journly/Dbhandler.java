package ly.journa.journly;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ly.journa.journly.Class.PojoAlarm;
import ly.journa.journly.Class.PojoPasscode;

/**
 * Created by acer on 9/28/2017.
 */

public class Dbhandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "journa_ly";

    // table name
    private static final String TABLE_LIST = "tbl_code";
    private static final String TABLE_ALARM = "tbl_alarm";

    // Table Columns names
    private static final String PASSCODE = "passcode";
    private static final String ALARM = "alarm";
    private static final String U_CODE = "u_code";
    private static final String KEY_ID = "_id";


   private static final String CREATE_LIST_TABLE = "CREATE TABLE " + TABLE_LIST + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + PASSCODE + " TEXT,"
            +  U_CODE + " TEXT" + ")";

    private static final String CREATE_ALARM_TABLE = "CREATE TABLE " + TABLE_ALARM + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + ALARM + " TEXT,"
            +  U_CODE + " TEXT" + ")";



    public Dbhandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ALARM_TABLE);
        db.execSQL(CREATE_LIST_TABLE);
        db.execSQL("INSERT INTO " + TABLE_ALARM + "("+ ALARM +","+U_CODE+") VALUES(0, 'donah')");
        db.execSQL("INSERT INTO " + TABLE_LIST + "("+ PASSCODE +","+U_CODE+") VALUES(0, 'donah')");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARM);
        // Create tables again
        onCreate(db);
    }


    // Getting All data
    public List<PojoPasscode> getPasscode() {
        List<PojoPasscode> pList = new ArrayList<PojoPasscode>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + TABLE_LIST + " ORDER BY " ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_LIST, null, null, null, null, null, null, null);
        // Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PojoPasscode pojodata = new PojoPasscode();
                pojodata.set_id(Integer.parseInt(cursor.getString(0)));
                pojodata.setPasscode(cursor.getString(1));

                pList.add(pojodata);
            } while (cursor.moveToNext());
        }

        // return contact list
        return pList;
    }
    public List<PojoAlarm> getAlarm() {
        List<PojoAlarm> pList1 = new ArrayList<PojoAlarm>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + TABLE_LIST + " ORDER BY " ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_ALARM, null, null, null, null, null, null, null);
        // Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PojoAlarm pojoAlarm = new PojoAlarm();
                pojoAlarm.set_id(Integer.parseInt(cursor.getString(0)));
                pojoAlarm.setAlarm(cursor.getString(1));

                pList1.add(pojoAlarm);
            } while (cursor.moveToNext());
        }

        // return contact list
        return pList1;
    }




    public int updatePasscode(String passcode, String u__code) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PASSCODE, passcode);
        // updating row
        return db.update(TABLE_LIST, values,"u_code = ?",
                new String[] {u__code});
    }

    public int updateAlarm(String alarm, String a__code) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ALARM, alarm);

        // updating row
        return db.update(TABLE_ALARM, values,"u_code = ?",
                new String[] {a__code});
    }

}
