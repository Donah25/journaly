package ly.journa.journly;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by acer on 9/28/2017.
 */

public class AddPinCodeActivity extends AppCompatActivity {

    TextView set_tv, ok_set;
    EditText p1, p2, p3, p4;
    Typeface tf, tf1;
    Dbhandler dbhandler;
    String p1_, p2_, p3_, p4_;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createpin);

        dbhandler = new Dbhandler(AddPinCodeActivity.this);

        set_tv = (TextView)findViewById(R.id.set_passcode);
        ok_set = (TextView) findViewById(R.id.ok_set);

        p1 = (EditText) findViewById(R.id.P1);
        p2 = (EditText) findViewById(R.id.P2);
        p3 = (EditText) findViewById(R.id.P3);
        p4 = (EditText) findViewById(R.id.P4);


        tf = Typeface.createFromAsset(getAssets(), "fontBold.ttf");
        tf1 = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        p1.setTypeface(tf);
        p2.setTypeface(tf);
        p3.setTypeface(tf);
        p4.setTypeface(tf);

        set_tv.setTypeface(tf1);
        ok_set.setTypeface(tf);

        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    p1.clearFocus();
                    p2.requestFocus();

                }else {
                    p1.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };
        TextWatcher textWatcher2 = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    p2.clearFocus();
                    p3.requestFocus();

                }
                else {
                    p2.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };
        TextWatcher textWatcher3 = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    p3.clearFocus();
                    p4.requestFocus();
                }else{
                    p3.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };

        p1.addTextChangedListener(textWatcher1);
        p2.addTextChangedListener(textWatcher2);
        p3.addTextChangedListener(textWatcher3);

        ok_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p1_ = p1.getText().toString().trim();
                p2_ = p2.getText().toString().trim();
                p3_ = p3.getText().toString().trim();
                p4_ = p4.getText().toString().trim();
                String pin = p1_ + p2_ + p3_ + p4_;

                Integer updateRows = dbhandler.updatePasscode(pin, "donah");

                if (updateRows>0){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddPinCodeActivity.this);

                    // Setting Dialog Message
                    alertDialog.setMessage("You've successfully set your pin.");

                    // Setting Negative "NO" Button
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(AddPinCodeActivity.this, HomeActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                }else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddPinCodeActivity.this);

                    // Setting Dialog Message
                    alertDialog.setMessage("Please try again.");

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.cancel();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                }


            }
        });
    }
}
