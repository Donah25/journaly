package ly.journa.journly;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.gms.wallet.fragment.SupportWalletFragment;
import com.google.android.gms.wallet.fragment.WalletFragmentInitParams;
import com.google.android.gms.wallet.fragment.WalletFragmentMode;
import com.google.android.gms.wallet.fragment.WalletFragmentOptions;
import com.google.android.gms.wallet.fragment.WalletFragmentStyle;


import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ly.journa.journly.Class.Constant;

/**
 * Created by acer on 9/27/2017.
 */

public class UpgradeActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button stripe_pay;
    private SupportWalletFragment mWalletFragment;
    Typeface tf;
    TextView amount_;
    Integer amount;
    String name;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade);
        setTitle("");

        toolbar = (Toolbar) findViewById(R.id.toolbar_upgrade);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        stripe_pay = (Button) findViewById(R.id.android_pay);
        amount_ = (TextView) findViewById(R.id.amount);


        tf = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        stripe_pay.setTypeface(tf);
        amount_.setTypeface(tf);




        stripe_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpgradeActivity.this);
                LayoutInflater inflater = UpgradeActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.activity_stripe, null);
                dialogBuilder.setView(dialogView);

                TextView stripe_tv = (TextView) dialogView.findViewById(R.id.stripe_tv);
                final EditText card_number = (EditText) dialogView.findViewById(R.id.card_number);
                final EditText expires = (EditText) dialogView.findViewById(R.id.expires);
                final EditText card_name = (EditText) dialogView.findViewById(R.id.card_name);
                final EditText cvc = (EditText) dialogView.findViewById(R.id.cvc);

                stripe_tv.setTypeface(tf);
                card_number.setTypeface(tf, Typeface.ITALIC);
                expires.setTypeface(tf, Typeface.ITALIC);
                card_name.setTypeface(tf, Typeface.ITALIC);
                cvc.setTypeface(tf, Typeface.ITALIC);

                dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {


                    }
                });
                dialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });
    }





    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}