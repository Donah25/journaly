package ly.journa.journly;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import ly.journa.journly.Class.Constant;
import ly.journa.journly.Class.PojoAlarm;
import ly.journa.journly.Class.PojoPasscode;
import ly.journa.journly.Class.Users;

public class MainActivity extends AppCompatActivity {

    Button login;
    TextView des, journal, forgot_ps, go_signup, error_msg;
    EditText et_email, et_password;
    Typeface tf, tf1;
    Dbhandler dbhandler;
    String EMAIL, PASSWORD;
    List<Users> users = new ArrayList<Users>();
    SessionManagement sessionManagement;
    String email = "", status;
    SessionManager sess;
    RelativeLayout no_account;
    Animation fadein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //  startService(new Intent(this, NotificationService.class));
        dbhandler = new Dbhandler(MainActivity.this);
        sessionManagement = new SessionManagement(getApplicationContext());
        sess = new SessionManager(getApplicationContext());


        if (sess.isLoggedIn())
        {
            Intent i = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(i);
        }

        final List<PojoAlarm> pData = dbhandler.getAlarm();
        final List<PojoPasscode> pcode = dbhandler.getPasscode();

        for (PojoPasscode ps : pcode) {
            if (!ps.getPasscode().equals("0")) {
                Intent i = new Intent(MainActivity.this, PasscodeActivity.class);
                startActivity(i);
                finish();
            }
            System.out.println("PIN  -------->" + ps.getPasscode());
      }
        for (PojoAlarm cn : pData) {
            if (cn.getAlarm().equals("1")){
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 20);
                calendar.set(Calendar.MINUTE, 00);

                Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            }
            System.out.println("Status -----> " + cn.getAlarm());
        }

        fadein = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        journal = (TextView) findViewById(R.id.journaly);
        des = (TextView) findViewById(R.id.des);
        go_signup = (TextView) findViewById(R.id.go_sign_up);
        forgot_ps = (TextView) findViewById(R.id.forgot_password);
        et_email = (EditText) findViewById(R.id.Email);
        et_password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.btn_login);
        error_msg = (TextView)findViewById(R.id.error_msg);
        no_account = (RelativeLayout)findViewById(R.id.no_account);

        tf = Typeface.createFromAsset(getAssets(), "fontBold.ttf");
        tf1 = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        journal.setTypeface(tf);
        des.setTypeface(tf1);
        et_email.setTypeface(tf1, Typeface.ITALIC);
        et_password.setTypeface(tf1, Typeface.ITALIC);
        login.setTypeface(tf);
        forgot_ps.setTypeface(tf1);
        go_signup.setTypeface(tf1);
        error_msg.setTypeface(tf1);


        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo nf = manager.getActiveNetworkInfo();

        if (nf == null) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

            // Setting Dialog Title
            alertDialog.setTitle("Alert!");

            // Setting Dialog Message
            alertDialog.setMessage("Please check your internet connection.");

            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    dialog.cancel();
                }
            });

            // Showing Alert Message
            alertDialog.show();
        } else {


            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();*/
                    EMAIL = et_email.getText().toString().trim();
                    PASSWORD = et_password.getText().toString().trim();

                    if (TextUtils.isEmpty(EMAIL)) {
                        et_email.setError("This field is required.");
                        return;
                    }

                    if (TextUtils.isEmpty(PASSWORD)) {
                        et_password.setError("This field is required.");
                        return;
                    }

                    if (!EMAIL.isEmpty() && !PASSWORD.isEmpty()) {

                        LoginUser b = new LoginUser();
                        b.execute(EMAIL, PASSWORD);

                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Please enter the credentials!", Toast.LENGTH_SHORT)
                                .show();

                    }
                }
            });

        }


        forgot_ps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ForgotPassword.class);
                startActivity(intent);
            }
        });

        go_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        //new BackGround().execute();

    }

    class LoginUser extends AsyncTask<String, String, String>{

        ProgressDialog progressDialog1 = new ProgressDialog(MainActivity.this);
        String stream = null;
       String EMAIL_, PS_;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog1.setMessage("Logging in...");
            progressDialog1.show();
        }

        @Override
        protected String doInBackground(String... params) {

            EMAIL_ = params[0];
            PS_ = params[1];
            String data = "";
            int tmp;


            Log.e("STRING", EMAIL_ + PS_);


            try {
                URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/login");
                String urlParams = "email="+EMAIL_+"&password="+PS_;


                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }


        }



        @Override
        protected void onPostExecute(final String response) {
            super.onPostExecute(response);

            try {

                JSONObject json = new JSONObject(response);


                    String status = json.getString("status");


                if(status.equals("0")){
                     no_account.startAnimation(fadein);
                     no_account.setVisibility(View.VISIBLE);
                     error_msg.setText("Your email or password is incorrect");
                     Log.e("NO USERRRR---------->", "NO USER");

                    //Toast.makeText(MainActivity.this, "Your email or password is incorrect", Toast.LENGTH_SHORT).show();
                }else if (status.equals("1")){

                        JSONObject created_date = json.getJSONObject("result");
                        final String cd_ = created_date.getString("created_date");

                        Log.e("CREATED_DATE", cd_);

                        sessionManagement.createLoginSession(EMAIL_, PS_, cd_);
                        sess.setLogin(true);
                        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    }else if(status.equals("2")) {

                        JSONObject created_date = json.getJSONObject("result");
                        final String cd_ = created_date.getString("created_date");

                        Log.e("CREATED_DATE", cd_);

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.new_password, null);
                        dialogBuilder.setView(dialogView);

                        final EditText new_pass = (EditText) dialogView.findViewById(R.id.new_password);
                        TextView ps_tv = (TextView) dialogView.findViewById(R.id.changeps_tv);

                        new_pass.setTypeface(tf1, Typeface.ITALIC);
                        ps_tv.setTypeface(tf1);

                        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                //new UpdatePassword(edt.getText().toString().trim(), EMAIL_).execute(Constant.getAddressUsers(response));
                                UpdatePassword up = new UpdatePassword();
                                up.execute(new_pass.getText().toString().trim(), EMAIL_, cd_);
                            }
                        });
                        dialogBuilder.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog b = dialogBuilder.create();
                        b.show();
                    }



                    et_email.setText("");
                    et_password.setText("");




            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }




            progressDialog1.dismiss();
        }

    }

    class UpdatePassword extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);

        String PASSWORD_, EMAIL_THIS, CD_;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading...");
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            PASSWORD_ = params[0];
            EMAIL_THIS = params[1];
            CD_ = params[2];
            String data = "";
            int tmp;


            Log.e("STRING", EMAIL_THIS + PASSWORD_ );


            try {
                URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/updatePassword");
                String urlParams = "email="+EMAIL_THIS+"&password="+PASSWORD_ +"&passcode="+"0";


                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }


        }


        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            progressDialog.dismiss();

            Log.e("STATUS", response);

            if (response.equals("1")){
                sessionManagement.createLoginSession(EMAIL_THIS, PASSWORD_, CD_);
                sess.setLogin(true);
                progressDialog.dismiss();
                Intent i = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }else {
                Toast.makeText(MainActivity.this, "Password was not updated!", Toast.LENGTH_SHORT).show();
            }


        }


    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
