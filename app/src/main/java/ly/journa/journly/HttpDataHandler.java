package ly.journa.journly;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Created by acer on 9/27/2017.
 */

public class HttpDataHandler {

    static String stream = null;

    public  HttpDataHandler(){
    }

    public String GetHttpData(String urlString) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            if (urlConnection.getResponseCode() == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    stringBuilder.append(line);
                stream = stringBuilder.toString();
                urlConnection.disconnect();
            } else {

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  stream;
    }

    public String GetHttpUser(String urlString, String email, String ps) {
        String data = "";
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            if (urlConnection.getResponseCode() == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    stringBuilder.append(line);
                stream = stringBuilder.toString();
                urlConnection.disconnect();

                try {

                    JSONArray json = new JSONArray(stream);

                    for (int i = 0; i < json.length(); i++) {

                        JSONObject obj = json.getJSONObject(i);
                        String emailj = obj.getString("email");
                        String psj = obj.getString("password");
                        String passcode = obj.getString("passcode");
                        String created_date = obj.getString("created_date");

                        JSONObject _id = obj.getJSONObject("_id");
                        String oid = _id.getString("$oid");


                        if (emailj.equals(email) && psj.equals(ps) && passcode.equals("0")) {

                            data = "1";


                        }else if (emailj.equals(email) && psj.equals(ps) && passcode.equals("1")){
                            data = oid;

                        }else{
                            data = "";

                        }
                    }
                } catch (JSONException e) {
                    Log.e("MYAPP", "unexpected JSON exception", e);
                    // Do something to recover ... or kill the app.
                }

                Log.e("StringGetUserHttpp", stream);

            } else {

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  data;
    }

    public String GetHttpEmail(String urlString, String email) {

        String data = "";
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            if (urlConnection.getResponseCode() == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    stringBuilder.append(line);
                stream = stringBuilder.toString();
                urlConnection.disconnect();

                try {

                    JSONArray json = new JSONArray(stream);

                    for (int i = 0; i < json.length(); i++) {

                        JSONObject obj = json.getJSONObject(i);
                        String emailj = obj.getString("email");
                        //String psj = obj.getString("password");

                        JSONObject _id = obj.getJSONObject("_id");
                        String oid = _id.getString("$oid");


                        if (emailj.equals(email)) {

                           data = oid;
                        }
                    }
                }
                    catch (JSONException e) {
                        Log.e("MYAPP", "unexpected JSON exception", e);
                        // Do something to recover ... or kill the app.
                    }

                } else {

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  data;
    }


    public void PostHttpData(String urlString, String json){
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();

            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            byte[] out = json.getBytes(StandardCharsets.UTF_8);
            int length = out.length;

            urlConnection.setFixedLengthStreamingMode(length);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.connect();
            try(OutputStream outputStream = urlConnection.getOutputStream()) {
              outputStream.write(out);
            }
            InputStream response = urlConnection.getInputStream();

        }catch (MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public void PutHttpData(String urlString, String newValue){
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();

            urlConnection.setRequestMethod("PUT");
            urlConnection.setDoOutput(true);

            byte[] out = newValue.getBytes(StandardCharsets.UTF_8);
            int length = out.length;

            urlConnection.setFixedLengthStreamingMode(length);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset-UTF-8");
            urlConnection.connect();
            try(OutputStream outputStream = urlConnection.getOutputStream()) {
                outputStream.write(out);
            }
            InputStream response = urlConnection.getInputStream();

        }catch (MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void DeleteHttpData(String urlString, String json){
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();

            urlConnection.setRequestMethod("DELETE");
            urlConnection.setDoOutput(true);

            byte[] out = json.getBytes(StandardCharsets.UTF_8);
            int length = out.length;

            urlConnection.setFixedLengthStreamingMode(length);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset-UTF-8");
            urlConnection.connect();
            try(OutputStream outputStream = urlConnection.getOutputStream()) {
                outputStream.write(out);
            }
            InputStream response = urlConnection.getInputStream();

        }catch (MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
