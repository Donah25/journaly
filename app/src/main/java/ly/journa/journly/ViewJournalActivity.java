package ly.journa.journly;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorResourceLoader;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

/**
 * Created by acer on 9/30/2017.
 */

public class ViewJournalActivity extends AppCompatActivity {

    TextView title, thoughts, date;
    Toolbar toolbar;
    Typeface typeface, typeface1;
    ImageView imageView;
    Button btn_edit;
    StorageReference storageReference;
    FirebaseStorage storage;
    Intent intent;
    String image_name;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_journal);
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_view);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReferenceFromUrl("gs://journaly-8d2fc.appspot.com/");


        intent = ViewJournalActivity.this.getIntent();

        title = (TextView)findViewById(R.id.title_view);
        thoughts = (TextView)findViewById(R.id.thought_view);
        date = (TextView)findViewById(R.id.date_view);
        imageView = (ImageView)findViewById(R.id.photo);
        btn_edit = (Button) findViewById(R.id.edit_btn);

        typeface = Typeface.createFromAsset(getAssets(), "perpetua.ttf");
        typeface1 = Typeface.createFromAsset(getAssets(), "fontBold.ttf");

        title.setText(intent.getStringExtra("title"));

        if (!intent.getStringExtra("thoughts").contains(intent.getStringExtra("img_name"))) {

            String f_thoughts = intent.getStringExtra("thoughts").replace(intent.getStringExtra("img_name"), "");

            thoughts.setText(f_thoughts.trim());
        }
        String date1 = intent.getStringExtra("date");
        String part[] = date1.split("-");
        String datetv = part[0] +" "+ part[1];

        String[] img_part = intent.getStringExtra("img_name").split(Pattern.quote("."));
        image_name = img_part[0];
        Log.e("LOGGGG --->>", img_part[0]);

        date.setText(datetv);


        date.setTypeface(typeface);
        thoughts.setTypeface(typeface);
        title.setTypeface(typeface1);
        btn_edit.setTypeface(typeface);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ViewJournalActivity.this, EditJournalActivity.class);
                i.putExtra("title", intent.getStringExtra("title"));
                i.putExtra("thoughts", intent.getStringExtra("thoughts"));
                i.putExtra("date", intent.getStringExtra("date"));
                i.putExtra("img_name", intent.getStringExtra("img_name"));
                i.putExtra("oid", intent.getStringExtra("oid"));
                startActivity(i);
            }
        });

        StorageReference imagesRef = storageReference.child("images");

        // Points to "images/space.jpg"
        // Note that you can use variables to create child values
        String fileName = intent.getStringExtra("img_name");
        StorageReference spaceRef = imagesRef.child(fileName);

        // File path is "images/space.jpg"
        String path = spaceRef.getPath();

        // File name is "space.jpg"
        String name = spaceRef.getName();

        // Points to "images"
        imagesRef = spaceRef.getParent();

        try {
            final File localFile = File.createTempFile("images", "jpg");
            spaceRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot >() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    //imageView.setImageBitmap(myBitmap);

                  /*  Drawable img = new BitmapDrawable(getResources(), myBitmap);
                    img.setBounds(0, 0, img.getIntrinsicWidth(), img.getIntrinsicHeight());
                    ss.setSpan(new ImageSpan(img, ImageSpan.ALIGN_BASELINE), 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    thoughts.setText(ss); */


                    String abc = intent.getStringExtra("thoughts");
                    SpannableStringBuilder ss = new SpannableStringBuilder(abc);

                    Drawable d;
                    ImageSpan span;


                        if (abc.contains(intent.getStringExtra("img_name"))) {


                            int startSpan = abc.indexOf(intent.getStringExtra("img_name"));
                            d = new BitmapDrawable(getResources(), myBitmap);
                            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                            span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                            ss.setSpan(span, startSpan, startSpan + intent.getStringExtra("img_name").length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

                            Log.e("THOUGHTSSS", abc);
                        }

                        thoughts.setText(ss);




                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        } catch (IOException e ) {}



       /* Glide.with(thoughts.getContext())
                .using(new FirebaseImageLoader())
                .load(spaceRef)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(300,300) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        thoughts.setCompoundDrawablesWithIntrinsicBounds(null, new BitmapDrawable(thoughts.getResources(),resource), null, null);
                    }
                });

                */
    }



    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
