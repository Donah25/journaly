package ly.journa.journly;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import ly.journa.journly.Class.PojoPasscode;

/**
 * Created by acer on 9/28/2017.
 */

public class PasscodeActivity extends AppCompatActivity {

    EditText p1, p2, p3, p4;
    TextView pc_tv, pc_forgot;
    Typeface tf, tf1;
    Dbhandler dbh;
    String p1_, p2_, p3_, p4_;
    String pin_saved;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passcode);

        dbh = new Dbhandler(PasscodeActivity.this);
        final List<PojoPasscode> pcode = dbh.getPasscode();

        p1 = (EditText)findViewById(R.id.Pin1);
        p2 = (EditText)findViewById(R.id.Pin2);
        p3 = (EditText)findViewById(R.id.Pin3);
        p4 = (EditText)findViewById(R.id.Pin4);

        pc_tv = (TextView)findViewById(R.id.enter_passcode);
        pc_forgot = (TextView) findViewById(R.id.forgot_passcode);

        tf = Typeface.createFromAsset(getAssets(), "fontBold.ttf");
        tf1 = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        p1.setTypeface(tf);
        p2.setTypeface(tf);
        p3.setTypeface(tf);
        p4.setTypeface(tf);


        pc_tv.setTypeface(tf1);
        pc_forgot.setTypeface(tf1);


        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    p1.clearFocus();
                    p2.requestFocus();

                }else {
                    p1.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };
        TextWatcher textWatcher2 = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    p2.clearFocus();
                    p3.requestFocus();

                }
                else {
                    p2.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };
        TextWatcher textWatcher3 = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    p3.clearFocus();
                    p4.requestFocus();
                }else{
                    p3.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };

        for (PojoPasscode ps : pcode) {
            pin_saved = ps.getPasscode();
        }


        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    p1_ = p1.getText().toString().trim();
                    p2_ = p2.getText().toString().trim();
                    p3_ = p3.getText().toString().trim();
                    p4_ = p4.getText().toString().trim();
                    String pin = p1_ + p2_ + p3_ + p4_;

                    if (pin_saved.equals(pin)){
                        Intent i = new Intent(PasscodeActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    }else{
                        p1.setText("");
                        p2.setText("");
                        p3.setText("");
                        p4.setText("");
                        p1.requestFocus();


                    }

                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };


        p1.addTextChangedListener(textWatcher1);
        p2.addTextChangedListener(textWatcher2);
        p3.addTextChangedListener(textWatcher3);
        p4.addTextChangedListener(textWatcher);





    }
}
