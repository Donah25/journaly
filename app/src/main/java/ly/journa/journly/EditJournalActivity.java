package ly.journa.journly;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import io.github.rockerhieu.emojicon.EmojiconEditText;
import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;
import ly.journa.journly.Class.Constant;

/**
 * Created by acer on 10/2/2017.
 */

public class EditJournalActivity extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener{

    Intent intent;
    EmojiconEditText edit_title, edit_thoughts;
    TextView date_edit;
    Button btn_edit;
    ImageView attach_edit, iv_photo, showImg;
    Typeface tf, tf1;
    StorageReference storageReference;
    FirebaseStorage storage;
    Toolbar toolbar;
    private static final int IMG_RESULT = 234;
    Uri filePath;
    private StorageReference mStorageRef;
    String image_name = "";
    String ImageDecode;
    String title, thoughts, email;
    SessionManagement session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_journal);
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_edit);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        session = new SessionManagement(EditJournalActivity.this);

        HashMap<String, String> user = session.getUserDetails();

        email = user.get(SessionManagement.KEY_EMAIL);

        intent = EditJournalActivity.this.getIntent();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReferenceFromUrl("gs://journaly-8d2fc.appspot.com/");

        mStorageRef = FirebaseStorage.getInstance().getReference();

        edit_title = (EmojiconEditText) findViewById(R.id.title_edit);
        edit_thoughts = (EmojiconEditText) findViewById(R.id.thought_edit);
        date_edit = (TextView)findViewById(R.id.date_edit);
        btn_edit = (Button) findViewById(R.id.save_btn_edit);
        attach_edit = (ImageView) findViewById(R.id.attach_photo_edit);
        iv_photo = (ImageView) findViewById(R.id.photo_edit);

        tf= Typeface.createFromAsset(getAssets(), "perpetua.ttf");
        tf1 = Typeface.createFromAsset(getAssets(), "fontBold.ttf");

        date_edit.setTypeface(tf);
        edit_thoughts.setTypeface(tf);
        edit_title.setTypeface(tf1);
        btn_edit.setTypeface(tf);

        String date1 = intent.getStringExtra("date");
        String part[] = date1.split("-");
        String datetv = part[0] +" "+ part[1];

        date_edit.setText(datetv);
        edit_title.setText(intent.getStringExtra("title"));


        StorageReference imagesRef = storageReference.child("images");

        String fileName = intent.getStringExtra("img_name");
        StorageReference spaceRef = imagesRef.child(fileName);


        try {
            final File localFile = File.createTempFile("images", "jpg");
            spaceRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot >() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    //imageView.setImageBitmap(myBitmap);

                  /*  Drawable img = new BitmapDrawable(getResources(), myBitmap);
                    img.setBounds(0, 0, img.getIntrinsicWidth(), img.getIntrinsicHeight());
                    ss.setSpan(new ImageSpan(img, ImageSpan.ALIGN_BASELINE), 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    thoughts.setText(ss); */


                    String abc = intent.getStringExtra("thoughts");
                    SpannableStringBuilder ss = new SpannableStringBuilder(abc);

                    Drawable d;
                    ImageSpan span;


                    if (abc.contains(intent.getStringExtra("img_name"))) {


                        int startSpan = abc.indexOf(intent.getStringExtra("img_name"));
                        d = new BitmapDrawable(getResources(), myBitmap);
                        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                        span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                        ss.setSpan(span, startSpan, startSpan + intent.getStringExtra("img_name").length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

                        Log.e("THOUGHTSSS ", abc);
                    }

                    edit_thoughts.setText(ss);




                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        } catch (IOException e ) {}



    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {

            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                    && null != data) {


                filePath = data.getData();
                String[] FILE = { MediaStore.Images.Media.DATA };


                Cursor cursor = getContentResolver().query(filePath,
                        FILE, null, null, null);

                cursor.moveToFirst();


                int columnIndex = cursor.getColumnIndex(FILE[0]);
                ImageDecode = cursor.getString(columnIndex);
                cursor.close();

                image_name =ImageDecode.substring(ImageDecode.lastIndexOf("/") + 1);



                image_name =ImageDecode.substring(ImageDecode.lastIndexOf("/") + 1);

                Bitmap myBitmap = BitmapFactory.decodeFile(ImageDecode);

                Drawable mDrawable = new BitmapDrawable(getResources(), myBitmap);

                ImageSpan imageSpan = new ImageSpan(mDrawable);

                SpannableStringBuilder builder = new SpannableStringBuilder();

                builder.append(edit_thoughts.getText());

                int selStart = edit_thoughts.getSelectionStart();
                builder.replace(edit_thoughts.getSelectionStart(), edit_thoughts.getSelectionEnd(), image_name);

                mDrawable.setBounds(0, 0, myBitmap.getWidth()/3,  myBitmap.getHeight()/3);

                builder.setSpan(imageSpan, selStart, selStart + image_name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                edit_thoughts.setText(builder);


                Log.e("FILE:", image_name);

            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }

    }


    public  void  attach_photo_edit(View arg0){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select photo"), IMG_RESULT);
    }

    private void uploadFile(){
        StorageReference riversRef = mStorageRef.child("images/"+image_name);
        if (filePath != null) {
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            // double progress = (100.0 * taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                        }
                    });
        }

    }

    public void save_btn_edit(View arg0){

        thoughts = edit_thoughts.getText().toString().trim();
        title = edit_title.getText().toString().trim();

        StorageReference riversRef = mStorageRef.child("images/"+image_name);
        riversRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // File deleted successfully
                Log.e("result", "onSuccess: deleted file");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
                Log.e("result", "onFailure: did not delete file");
            }
        });

        uploadFile();

        if (image_name.isEmpty()){
            image_name = intent.getStringExtra("img_name");
        }

        PutData putData = new PutData();
        putData.execute(email, title, thoughts, image_name,intent.getStringExtra("date"), intent.getStringExtra("oid"));


    }

    class PutData extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog = new ProgressDialog(EditJournalActivity.this);
        String EMAIL_, TITLE_, THOUGHTS_, IMG_NAME_, DATE_, ID_;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Updating...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            EMAIL_ = params[0];
            TITLE_ = params[1];
            THOUGHTS_ = params[2];
            IMG_NAME_ = params[3];
            DATE_ = params[4];
            ID_ = params[5];
            String data = "";
            int tmp;



            try {
                URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/UpdateJournals");
                String urlParams = "email="+EMAIL_+"&title="+TITLE_+"&thoughts="+THOUGHTS_+"&img_name="+IMG_NAME_+"&created_date="+DATE_+"&journal_id="+ID_;


                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Intent i = new Intent(EditJournalActivity.this, HomeActivity.class);
            startActivity(i);
            finish();

            progressDialog.dismiss();
        }


    }

    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(edit_title, emojicon);
        EmojiconsFragment.input(edit_thoughts, emojicon);
    }

    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(edit_title);
        EmojiconsFragment.backspace(edit_thoughts);
    }

    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
