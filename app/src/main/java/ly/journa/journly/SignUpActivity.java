package ly.journa.journly;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import ly.journa.journly.Class.Constant;

/**
 * Created by acer on 9/27/2017.
 */

public class SignUpActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText email_sp, ps_sp;
    Button btn_sp;
    Typeface typeface;
    String EMAIL, PASSWORD;
    private ProgressDialog progressDialog;
    SessionManagement sessionManagement;
    SessionManager sess;
    String dateDb;
    TextView error_msg_signup;
    RelativeLayout relativeLayout;

    Animation fadein;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setTitle("");

        toolbar = (Toolbar) findViewById(R.id.toolbar_signup);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sessionManagement = new SessionManagement(getApplicationContext());
        sess = new SessionManager(getApplicationContext());

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        //yyyy-mm-dd
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        dateDb = df.format(c.getTime());


        email_sp = (EditText)findViewById(R.id.email_sp);
        ps_sp = (EditText)findViewById(R.id.ps_signup);
        btn_sp = (Button) findViewById(R.id.sign_up);
        error_msg_signup = (TextView) findViewById(R.id.error_msg_signup);
        relativeLayout = (RelativeLayout) findViewById(R.id.no_account_signup);

        typeface = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        fadein = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        email_sp.setTypeface(typeface);
        ps_sp.setTypeface(typeface);
        btn_sp.setTypeface(typeface);
        error_msg_signup.setTypeface(typeface);



        btn_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EMAIL = email_sp.getText().toString().trim();
                PASSWORD = ps_sp.getText().toString().trim();

                if (TextUtils.isEmpty(EMAIL)) {
                    email_sp.setError("This field is required.");
                    return;
                }

                if (TextUtils.isEmpty(PASSWORD)) {
                    ps_sp.setError("This field is required.");
                    return;
                }

                if (!EMAIL.isEmpty() && !PASSWORD.isEmpty()) {

                    PostData postData = new PostData();
                    postData.execute(EMAIL, PASSWORD, dateDb);

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_SHORT)
                            .show();

                }

            }
        });

    }



    class PostData extends AsyncTask<String, String, String>{

        ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this);
        String EMAIL_, PASSWORD_, DATE_;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Signing up...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            EMAIL_ = params[0];
            PASSWORD_ = params[1];
            DATE_ = params[2];
            String data = "";
            int tmp;

            try {
                URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/create_user");
                String urlParams = "email="+EMAIL_+"&password="+PASSWORD_+"&created_date="+DATE_;


                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            progressDialog.dismiss();

            if (response.equals("1")) {

                relativeLayout.startAnimation(fadein);
                relativeLayout.setVisibility(View.VISIBLE);
                error_msg_signup.setText("Email exists. Please try again.");


            }else {
                sessionManagement.createLoginSession(EMAIL_, PASSWORD_, DATE_);
                sess.setLogin(true);
                Intent i = new Intent(SignUpActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }

        }


    }

    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
