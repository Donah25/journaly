package ly.journa.journly;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ly.journa.journly.Class.Constant;
import ly.journa.journly.Class.Journals;

/**
 * Created by acer on 9/30/2017.
 */

public class CustomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<Journals> mdata = Collections.emptyList();
    String title_;


    public CustomAdapter(List<Journals> rdata) {
        this.mdata = rdata;

    }

    // create constructor to innitilize context and data sent from MainActivity
    public CustomAdapter(Context context, List<Journals> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.mdata = data;
    }

    // Inflate the layout when viewholder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.activity_row, parent, false);


        MyHolder holder = new MyHolder(view);
        return holder;
    }

    // Bind data
    @Override

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        final MyHolder myHolder = (MyHolder) holder;
        final Journals current = mdata.get(position);
        Typeface tf;

        tf = Typeface.createFromAsset(context.getAssets(), "perpetua.ttf");

        myHolder.title.setTypeface(tf);
        myHolder.date.setTypeface(tf);
        myHolder.delete.setTypeface(tf);

        myHolder.title.setText(current.journal_name);

        Log.e("IDDD --->>", current.journal_id);


        String[] dateP = current.date_created.split("-");
        myHolder.date.setText(dateP[0] + " " + dateP[1]);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ViewJournalActivity.class);
                intent.putExtra("title", current.journal_name);
                intent.putExtra("thoughts", current.thoughts);
                intent.putExtra("date", current.date_created);
                intent.putExtra("img_name", current.images_stock);
                intent.putExtra("oid", current.journal_id);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                // Setting Dialog Title
                alertDialog.setTitle("Alert!");

                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want delete this?");


                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {

                        DeleteData jData = new DeleteData();
                        jData.execute(current.journal_id);
                        mdata.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, mdata.size());
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
                return true;
            }
        });



        title_ = current.journal_name;
    }
    public void removeItem(int position) {
        final Journals current = mdata.get(position);
        Log.e("Current", current.journal_name);

        DeleteData jData = new DeleteData();
        jData.execute(current.journal_id);

        mdata.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mdata.size());
    }
    // return total item from List
    @Override
    public int getItemCount() {
        return mdata.size();
    }

    public List<Journals> getmData() {
        return mdata;
    }


    class MyHolder extends RecyclerView.ViewHolder {

        TextView title, date, delete;
        public RelativeLayout viewBackground, viewForeground;


        public MyHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date_sum);
            title = (TextView) itemView.findViewById(R.id.list_title);
            delete = (TextView) itemView.findViewById(R.id.delete_tv);
            viewBackground = (RelativeLayout) itemView.findViewById(R.id.view_background);
            viewForeground = (RelativeLayout) itemView.findViewById(R.id.view_foreground);

        }
    }

    public void setFilter(List<Journals> journalModels) {
        mdata = new ArrayList<>();
        mdata.addAll(journalModels);
        notifyDataSetChanged();
    }

    public void refreshItems(List<Journals> dataLoad){
        this.mdata = dataLoad;
        notifyDataSetChanged();
    }

    class DeleteData extends AsyncTask<String, String, String> {

        String ID_;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            ID_ = params[0];
            String data = "";
            int tmp;

            try {
                URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/DeleteJournals");
                String urlParams = "journal_id="+ID_;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }


    }

}
