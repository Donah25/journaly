package ly.journa.journly;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Px;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import io.github.rockerhieu.emojicon.EmojiconEditText;
import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;
import ly.journa.journly.Class.Constant;

/**
 * Created by acer on 9/29/2017.
 */

public class AddJournalActivity extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    View rootView;
    Typeface tf, tf1;
    TextView date, img_loc;
    Button save;
    ImageView imageView, showImg, emo1;
    EditText add_thoughts;
    String title, thoughts, email;
    String ImageDecode;
    EmojiconEditText add_title;
    private static final int IMG_RESULT = 234;
    private StorageReference mStorageRef;
    Uri filePath;
    String image_name, dateDb;
    SessionManagement session;
    FrameLayout frameLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_journal);
        setTitle("");

        session = new SessionManagement(AddJournalActivity.this);

        HashMap<String, String> user = session.getUserDetails();

        email = user.get(SessionManagement.KEY_EMAIL);


        mStorageRef = FirebaseStorage.getInstance().getReference();

        date = (TextView) findViewById(R.id.Date);
        save = (Button) findViewById(R.id.save_btn);
        imageView = (ImageView)findViewById(R.id.attach);
        add_thoughts = (EditText) findViewById(R.id.add_thoughts);
        add_title = (EmojiconEditText) findViewById(R.id.enter_title);
        img_loc = (TextView)findViewById(R.id.img_loc);
        //  emo1 = (ImageView)findViewById(R.id.emoji_icon);
        //frameLayout = (FrameLayout)findViewById(R.id.emojicons);

        tf = Typeface.createFromAsset(getAssets(), "fontBold.ttf");
        tf1 = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        date.setTypeface(tf1);
        add_title.setTypeface(tf);
        add_thoughts.setTypeface(tf1);
        save.setTypeface(tf);


        /*emo1.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(final View v) {
                //setEmojiconFragment(true);
                add_title.setUseSystemDefault(true);
               // setEmojiconFragment(true);
               // frameLayout.setVisibility(View.VISIBLE);
            }
        });*/

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy HH:mm:ss");
        SimpleDateFormat dfMd = new SimpleDateFormat("MMM dd");
        dateDb = df.format(c.getTime());
        String dateTv = dfMd.format(c.getTime());

        date.setText(dateTv);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    // for empty text color
                    save.setBackgroundDrawable(ContextCompat.getDrawable(AddJournalActivity.this, R.drawable.btn_grey));
                } else {
                    // for non empty field color
                    save.setBackgroundDrawable(ContextCompat.getDrawable(AddJournalActivity.this, R.drawable.btn_style));

                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };


        add_thoughts.addTextChangedListener(textWatcher);
        add_title.addTextChangedListener(textWatcher);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {

            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                    && null != data) {


                filePath = data.getData();
                String[] FILE = { MediaStore.Images.Media.DATA };


                Cursor cursor = getContentResolver().query(filePath,
                        FILE, null, null, null);

                cursor.moveToFirst();


                int columnIndex = cursor.getColumnIndex(FILE[0]);
                ImageDecode = cursor.getString(columnIndex);
                cursor.close();

                image_name =ImageDecode.substring(ImageDecode.lastIndexOf("/") + 1);

                img_loc.setText(ImageDecode);


                Bitmap myBitmap = BitmapFactory.decodeFile(ImageDecode);

                Drawable mDrawable = new BitmapDrawable(getResources(), myBitmap);

                ImageSpan imageSpan = new ImageSpan(mDrawable);

                SpannableStringBuilder builder = new SpannableStringBuilder();

                builder.append(add_thoughts.getText());

                int selStart = add_thoughts.getSelectionStart();
                builder.replace(add_thoughts.getSelectionStart(), add_thoughts.getSelectionEnd(), image_name);

                mDrawable.setBounds(0, 0, myBitmap.getWidth()/3,  myBitmap.getHeight()/3);

                builder.setSpan(imageSpan, selStart, selStart + image_name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                add_thoughts.setText(builder);


               /* Uri imageUri = data.getData();

                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                String myBase64Image = encodeToBase64(selectedImage, Bitmap.CompressFormat.JPEG, 100);

                System.out.println("BASE64 --->" + myBase64Image);

                Drawable d = new BitmapDrawable(getResources(), selectedImage);

                SpannableString ss = new SpannableString( "<img>" + myBase64Image + "</img>");
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                ss.setSpan(span, 0, ("<img>" + myBase64Image + "</img>").length() , Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                add_thoughts.setText(ss);
                */

                Log.e("FILE:", image_name);

            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }

    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }


    public  void  attach_photo(View arg0){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select photo"), IMG_RESULT);
    }

    private void uploadFile(){
        StorageReference riversRef = mStorageRef.child("images/"+image_name);
        if (filePath != null) {
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            // double progress = (100.0 * taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                        }
                    });
        }

    }

    public void save_btn(View arg0){

        thoughts = add_thoughts.getText().toString().trim();
        title = add_title.getText().toString().trim();

        uploadFile();

        JournalData jData = new JournalData();
        jData.execute(email, title, thoughts, dateDb, image_name);


    }


    class JournalData extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog = new ProgressDialog(AddJournalActivity.this);
        String EMAIL_, TITLE_, THOUGHTS_, DATE_, IMG_NAME_;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Adding...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {


            EMAIL_ = params[0];
            TITLE_ = params[1];
            THOUGHTS_ = params[2];
            DATE_ = params[3];
            IMG_NAME_ = params[4];
            String data = "";
            int tmp;



            try {
                URL url = new URL("http://165.227.82.131/journaly_api/api/index.php/add_journal");
                String urlParams = "email="+EMAIL_+"&title="+TITLE_+"&thoughts="+THOUGHTS_+"&img_name="+IMG_NAME_+"&created_date="+DATE_;


                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Intent i = new Intent(AddJournalActivity.this, HomeActivity.class);
            startActivity(i);
            finish();

            progressDialog.dismiss();
        }


    }




   /* private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();

    } */


    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(add_title, emojicon);
        EmojiconsFragment.input(add_thoughts, emojicon);
    }

    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(add_title);
        EmojiconsFragment.backspace(add_thoughts);
    }

    public void openEmojiconsActivity(View view) {
        startActivity(new Intent(this, AddJournalActivity.class));
    }


    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
