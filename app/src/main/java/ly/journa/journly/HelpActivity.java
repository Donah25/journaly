package ly.journa.journly;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

import java.util.HashMap;

import ly.journa.journly.Class.Constant;

/**
 * Created by acer on 10/17/2017.
 */

public class HelpActivity extends AppCompatActivity {

    Typeface tf;
    TextView txt_help;
    EditText et_msg;
    Button send;
    SessionManagement session;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        setTitle("");

        toolbar = (Toolbar) findViewById(R.id.toolbar_help);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        session = new SessionManagement(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();

        String PS = user.get(SessionManagement.KEY_PS);

        final String email_sess = user.get(SessionManagement.KEY_EMAIL);

        send = (Button) findViewById(R.id.send_btn);
        txt_help = (TextView) findViewById(R.id.text_help);
        et_msg = (EditText) findViewById(R.id.msg_help);

        tf = Typeface.createFromAsset(getAssets(), "perpetua.ttf");

        send.setTypeface(tf);
        txt_help.setTypeface(tf);
        et_msg.setTypeface(tf);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg_ = et_msg.getText().toString().trim();

                SendEmailASyncTask task = new SendEmailASyncTask(email_sess, msg_);
                task.execute();
            }
        });

    }

    class SendEmailASyncTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = new ProgressDialog(HelpActivity.this);

        private String mMsgResponse;

        String email_, msg;


        public SendEmailASyncTask(String email, String msg) {
            this.email_ = email;
            this.msg = msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Sending...");
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                SendGrid sendgrid = new SendGrid("jonwoon", "asdf1234");

                SendGrid.Email email = new SendGrid.Email();

                email.addTo("donahyoozlr@gmail.com");
                email.setFrom(email_);
                email.setSubject("Message from Customer");
                email.setText(msg);


                // Send email, execute http request
                SendGrid.Response response = sendgrid.send(email);
                mMsgResponse = response.getMessage();

                Log.d("SendAppExample", mMsgResponse);

            } catch (SendGridException e) {
                Log.e("SendAppExample", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            progressDialog.dismiss();

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(HelpActivity.this);

            // Setting Dialog Message
            alertDialog.setMessage("Message sent! \nPlease wait as we will get back to you soon.");

            // Setting Negative "NO" Button
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    Intent i = new Intent(HelpActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();

                }
            });

            alertDialog.show();

            //Toast.makeText(ForgotPassword.this, mMsgResponse, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
